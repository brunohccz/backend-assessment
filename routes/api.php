<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->group(function () {
    Route::get('employees', 'EmployeeController@index');
    Route::post('employees/csv', 'EmployeeController@csv');
    Route::delete('employees/{document}', 'EmployeeController@destroy');
});

Route::middleware('guest:api')->group(function () {
   Route::post('auth/login', 'Auth\LoginController@login');
   Route::post('auth/register', 'Auth\RegisterController@register');
});