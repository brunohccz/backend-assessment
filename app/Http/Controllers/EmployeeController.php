<?php

namespace App\Http\Controllers;

use App\Employee;
use App\Http\Requests\EmployeeImportRequest;
use App\Imports\EmployeesImport;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Excel as MaatwebsiteExcel;
use Maatwebsite\Excel\Facades\Excel;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $employees = Employee::all();

        return response()->json($employees);
    }

    /**
     * Import employees from csv
     * @param EmployeeImportRequest $request
     * @return Response
     */
    public function csv(EmployeeImportRequest $request)
    {
        Excel::import(new EmployeesImport($request->user()), $request->file('employees'), null, MaatwebsiteExcel::CSV);

        return response('', Response::HTTP_CREATED);
    }

    /**
     * Remove the specified resource from storage.
     * @param string $document
     * @return Response
     */
    public function destroy(string $document)
    {
        $employee = Employee::whereDocument($document)->firstOrFail();
        $employee->delete();

        return response()->noContent();
    }
}
