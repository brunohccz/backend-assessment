<?php

namespace App\Imports;

use App\Employee;
use App\Notifications\EmployeesImportNotification;
use App\User;
use Maatwebsite\Excel\Concerns\Importable;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Events\AfterImport;

class EmployeesImport implements ToModel, WithEvents, WithStartRow
{
    use Importable;

    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @return array
     */
    public function registerEvents(): array
    {
        return [
            AfterImport::class => function() {
                $this->user->notify(new EmployeesImportNotification);
            }
        ];
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        return 2;
    }

    /**
     * @param array $row
     * @return Employee
     */
    public function model(array $row): Employee
    {
        [$name, $email, $document, $city, $state, $start_date] = $row;

        $search = ['document' => $document];

        return Employee::updateOrCreate($search,[
            'name'       => $name ?? '',
            'email'      => $email ?? '',
            'document'   => $document ?? '',
            'city'       => $city ?? '',
            'state'      => $state ?? '',
            'start_date' => $start_date ?? '',
            'manager_id' => $this->user->id
        ]);
    }
}
