<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Uuid as RamseyUuid;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'id' => RamseyUuid::uuid4()->toString(),
            'name' => 'Dennis Ritchie',
            'email' => 'dennis.ritchie@convenia.com.br',
            'password' => Hash::make('C0nv3n!4')
        ]);
        DB::table('users')->insert([
            'id' => '41b6c1f7-1d2a-4050-a28d-4861a65d948f',
            'name' => 'Alan Turing',
            'email' => 'convenia-teste-1@discardmail.de',
            'password' => Hash::make('C0nv3n!4')
        ]);
        DB::table('users')->insert([
            'id' => RamseyUuid::uuid4()->toString(),
            'name' => 'Ada Lovelace',
            'email' => 'ada.lovelace@convenia.com.br',
            'password' => Hash::make('C0nv3n!4')
        ]);
    }
}
